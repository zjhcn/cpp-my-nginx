#include <stdio.h>
#include <unistd.h>
#include <signal.h>

extern void dont_kill_me1();
extern void dont_kill_me2();
extern void print_loop();

int main(int argc, char *const *argv)
{
  printf("start my nginx \n");

  dont_kill_me2();

  printf("end my nginx \n");
  return 0;
}

void print_loop()
{
  int count = 0;
  for (;;)
  {
    sleep(1);
    printf("loop count %d\n", count++); // \n 如果没有换行符 死循环不打印，内容一直在缓冲区中
  }
}

/**
 * 使用忽略关闭信号的方法，是进程存活
 * bash关掉后，进程就会成为孤儿进程，PPID变成 1
 **/
void dont_kill_me1()
{
  // 系统函数，设置某个信号来的时候处理方式
  signal(SIGHUP, SIG_IGN); // SIG_IGN 要求忽略对应的信号 SIGHUP
  print_loop();
}

/**
 * 新建一个session id使进程接收不到 bash关闭的信号
 * 如果进程是进程组的组长，直接调用setsid会没效果
 * 需要创建一个子进程再修改session id
 **/
void dont_kill_me2()
{
  __pid_t pid;

  pid = fork();
  if (pid < 0)
  {
    printf("子进程创建失败\n");
  }
  // 子进程
  else if (pid == 0)
  {
    setsid();
    print_loop();
  }
  // 父进程
  else
  {
    print_loop();
  }
}