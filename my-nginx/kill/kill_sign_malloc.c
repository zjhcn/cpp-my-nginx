#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

extern void kill_sign();
extern void print_loop();

int main(int argc, char *const *argv)
{
  printf("start my nginx \n");

  kill_sign();

  printf("end my nginx \n");
  return 0;
}

void print_loop()
{
  int count = 0;
  for (;;)
  {
    // sleep(1);
    // printf("loop count %d\n", count++); // \n 如果没有换行符 死循环不打印，内容一直在缓冲区中
    int *p = NULL;
    p = (int *)malloc(sizeof(int));
    free(p);
  }
}

void sig_usr(int sig)
{
  // 在信号处理函数使用不可重入函数，会使程序异常
  int *p = NULL;
  p = (int *)malloc(sizeof(int));
  free(p);

  if (sig == SIGUSR1)
  {
    printf("捕捉到信号usr1\n");
  }
  else if (sig == SIGUSR2)
  {
    printf("捕捉到信号usr2\n");
  }
  else
  {
    printf("未捕捉到信号\n");
  }
}

/**
 * 使用忽略关闭信号的方法，是进程存活
 * bash关掉后，进程就会成为孤儿进程，PPID变成 1
 **/
void kill_sign()
{
  // 系统函数，设置某个信号来的时候处理方式
  signal(SIGUSR1, sig_usr);
  signal(SIGUSR2, sig_usr);
  print_loop();
}
